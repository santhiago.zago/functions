package main

import (
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/edcastJitUserCreateProxy"
	"log"
	"net/http"

	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/RoundAndBalanceNetsuiteJE"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/hextobase64"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/json2yaml"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/techstackymltojson"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/zuorarevenue"
)

func main() {
	http.HandleFunc("/techstackymltojson", techstackymltojson.Index)

	http.HandleFunc("/zrevtonsje", zuorarevenue.TransferFileToNSJE)

	http.HandleFunc("/roundbalanceje", RoundAndBalanceNetsuiteJE.Index)

	http.HandleFunc("/json2yaml", json2yaml.Index)

	http.HandleFunc("/hextobase64", hextobase64.Index)

	http.HandleFunc("/edcastjit", edcastJitUserCreateProxy.Index)

	log.Println("Starting server on port 8181")
	log.Fatal(http.ListenAndServe(":8181", nil))
}

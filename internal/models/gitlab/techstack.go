package gitlab

//- title: 1Password
//  team_member_baseline_entitlement: true
//  subprocessor: null
//  description: "[1Password](https://1password.com/) is a password manager. GitLab
//    uses 1Passwords for Teams which is a hosted service. Please refer to the [1Password
//    Guide](/handbook/security/#1password-guide) for additional information."
//  access_to: Team Members
//  provisioner: 'Leave "Assignee" blank, tag the group @gitlab-com/business-ops/team-member-enablement in the comments and add the label IT::to do'
//  deprovisioner: IT Ops
//  group_owner_slack_channel: 'for help: #it_help in slack; IT Ops (Group Owner)'
//  business_owner: IT Operations
//  technical_owner: IT Operations
//  data_classification: Red
//  integration_method: null
//  need_move_to_okta: null
//  critical_systems_tier: null
//  in_sox_scope: null
//  date_added_to_sox_scope: null
//  collected_data: null
//  processing_customer_data: null
//  employee_or_customer_facing_app: null
//  notes: null
//  saas: null

type TechStackItem struct {
	Title string `yaml:"title"`
	TeamMemberBaselineEntitlement string `yaml:"team_member_baseline_entitlement"`
	Subprocessor bool `yaml:"subprocessor"`
	Description string `yaml:"description"`
	AccessTo string `yaml:"access_to"`
	Provisioner string `yaml:"provisioner"`
	Deprovisioner string `yaml:"deprovisioner"`
	GroupOwnerSlackChannel string `yaml:"group_owner_slack_channel"`
	BusinessOwner string `yaml:"business_owner"`
	TechnicalOwner string `yaml:"technical_owner"`
	DataClassification string `yaml:"data_classification"`
	IntegrationMethod string `yaml:"integration_method"`
	NeedMoveToOkta string `yaml:"need_move_to_okta"`
	CriticalSystemsTier string `yaml:"critical_systems_tier"`
	InSoxScope bool `yaml:"in_sox_scope"`
	DateAddedToSoxScope string `yaml:"date_added_to_sox_scope"`
	CollectedData string `yaml:"collected_data"`
	ProcessingCustomerData string `yaml:"processing_customer_data"`
	EmployeeOrCustomerFacingApp string `yaml:"employee_or_customer_facing_app"`
	Notes string `yaml:"notes"`
	Saas bool `yaml:"saas"`
}

package netsuite

import (
	"fmt"

	"github.com/shopspring/decimal"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/netsuite"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/zuora/revenue"
)

/*
Rounds to 2 decimal places taking into account the lost precision
At the end if the lost precision amounts to 0.01 or more a debit or credit value will be
added as a rounding adjustment to the JE in order to have it balance.
*/
func RoundAndBalanceJournalEntry(journalEntry netsuite.JournalEntry) netsuite.JournalEntry {
	je := netsuite.JournalEntry{
		Entity: journalEntry.Entity,
		Lines:  journalEntry.Lines,
	}

	creditRoundingDifference := decimal.NewFromFloat(0)
	debitRoundingDifference := decimal.NewFromFloat(0)

	for i, line := range journalEntry.Lines {
		if !line.AccountedCredit.IsZero() {
			// Round to 2 decimal places
			rounded := line.AccountedCredit.Round(2)
			// Calculate the lost accuracy

			// If result is negative that means that we over-accounted for revenue
			roundingDifference := line.AccountedCredit.Sub(rounded)

			// Save the rounded value where the old value was
			journalEntry.Lines[i].AccountedCredit = rounded

			// Accumulate the lost precision
			creditRoundingDifference = creditRoundingDifference.Add(roundingDifference)
		} else {
			// Round to 2 decimal places
			rounded := line.AccountedDebit.Round(2)
			// Calculate the lost accuracy
			roundingDifference := line.AccountedDebit.Sub(rounded)
			// Save the rounded value where the old value was
			journalEntry.Lines[i].AccountedDebit = rounded

			// Accumulate the lost precision
			debitRoundingDifference = debitRoundingDifference.Add(roundingDifference)
		}
	}

	fmt.Println(creditRoundingDifference)
	fmt.Println(debitRoundingDifference)

	roundingDifference := creditRoundingDifference.Sub(debitRoundingDifference)
	fmt.Println(roundingDifference)

	line := revenue.TransferBatchJournalLine{
		AccountingDate:        "",
		CurrencyCode:          journalEntry.Lines[0].CurrencyCode,
		Memo:                  "Rounding adjustment",
		Entity:                journalEntry.Entity,
		AccountingPeriodName:  journalEntry.Lines[0].AccountingPeriodName,
		GLAccountNumber:       "4011", // Rounding Gain/Loss account
		ProductRatePlanCharge: "",
	}

	fmt.Println("Rounding Difference:", roundingDifference)

	roundingDifference = roundingDifference.Round(2)

	fmt.Println("Rounding Difference:", roundingDifference)

	if roundingDifference.GreaterThanOrEqual(decimal.NewFromFloat(0.01)) || roundingDifference.LessThanOrEqual(decimal.NewFromFloat(-0.01)) {
		if roundingDifference.IsPositive() {
			line.AccountedCredit = roundingDifference.Abs()
		} else {
			line.AccountedDebit = roundingDifference.Abs()
		}

		je.Lines = append(je.Lines, line)
	}

	return je
}

func SumJournalEntry(entry netsuite.JournalEntry) decimal.Decimal {
	sum := decimal.NewFromFloat(0)

	for _, line := range entry.Lines {
		if line.AccountedCredit.IsPositive() {
			sum = sum.Add(line.AccountedCredit)
		}

		if line.AccountedDebit.IsPositive() {
			sum = sum.Sub(line.AccountedDebit)
		}
	}

	return sum
}

package zuorarevenue

import (
	"encoding/json"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/zuora/revenue"
	http2 "gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/http"
	revenue2 "gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/zuora/revenue"
	"io/ioutil"
	"net/http"
)

func TransferFileToNSJE(w http.ResponseWriter, r *http.Request) {
	transferFileBatch := new(revenue.TransferFileBatch)

	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
		return
	}

	err = json.Unmarshal(data, transferFileBatch)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
		return
	}

	summaryJournalEntries := revenue2.BatchToNSJEList(transferFileBatch.JournalLines)

	jsonResponse, _ :=json.MarshalIndent(summaryJournalEntries, "", "  ")

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonResponse)
}

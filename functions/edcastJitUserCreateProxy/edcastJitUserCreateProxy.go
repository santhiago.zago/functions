package edcastJitUserCreateProxy

import (
	"fmt"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/edcast"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/services/edcastservice"
	"log"
	"net/http"
	"os"
	"regexp"
)

func StatusError(w http.ResponseWriter) {
	errorPage := `
	<html>
		<body>
			<h1>500</h1>
			<p>An error occurred while fetching your EdCast login URL. Please contact your system administrator for assistance. Thank you</p>
		</body>
	</html>
`

	w.Write([]byte(errorPage))
	w.Header().Set("Content-Type", "text/html")
}

func SuccessPage(w http.ResponseWriter) {
	successPage := fmt.Sprintf(`
	<html>
		<body>
			<h1>Success</h1>
			<p>Your EdCast account was successfully created, please check your email for the invitation link</p>

			<a href="%s/log_in">Click here to access edcast</a>
		</body>
	</html>
`, os.Getenv(edcastservice.BaseUrl))

	w.Write([]byte(successPage))
	w.Header().Set("Content-Type", "text/html")
}

func Index(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	pattern := os.Getenv(edcastservice.AllowedReferrer)

	if len(pattern) == 0 {
		log.Println("EDCAST_ALLOWED_REFERRER was empty")
		StatusError(w)
		return
	}

	match, err := regexp.Match(pattern, []byte(r.Referer()))
	if !match {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	queryValues := r.URL.Query()

	u := &edcast.User{
		Email:            queryValues.Get("email"),
		FirstName:        queryValues.Get("first_name"),
		LastName:         queryValues.Get("last_name"),
		Id:               queryValues.Get("id"),
		Status: 		  edcast.StatusActive,
		Roles: []string{
			edcast.RoleMember,
		},
		ImpartnerAccount: true,
		SendEmailNotification: true,
	}

	userExists, err := edcastservice.DoesUserExist(u.Id)
	if err != nil {
		StatusError(w)
		return
	}

	if userExists {
		w.Header().Set("Location", fmt.Sprintf("%s/log_in", os.Getenv(edcastservice.BaseUrl)))
		w.WriteHeader(http.StatusTemporaryRedirect)
		return
	}

	err = edcastservice.CreateUser(u)
	if err != nil {
		StatusError(w)
		return
	}

	SuccessPage(w)
	return
}

package revenue

import (
	"encoding/json"
	"testing"

	"github.com/shopspring/decimal"
	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/netsuite"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/zuora/revenue"
)

func TestGroupByEntity(t *testing.T) {
	batch := revenue.TransferFileBatch{
		BatchCount: 5,
		BatchId:    1,
		JournalLines: []revenue.TransferBatchJournalLine{
			{
				AccountedCredit:       decimal.NewFromFloat(100.55),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(1000.23),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloat(500.10),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloat(200),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "AUS",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(100),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "AUS",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
		},
	}

	journalEntries, _ := GroupByEntity(batch)

	assert.Contains(t, journalEntries, netsuite.JournalEntry{
		Entity: "AUS",
		Lines: []revenue.TransferBatchJournalLine{
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloat(200),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "AUS",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(100),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "AUS",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
		},
	})

	assert.Contains(t, journalEntries, netsuite.JournalEntry{
		Entity: "US",
		Lines: []revenue.TransferBatchJournalLine{
			{
				AccountedCredit:       decimal.NewFromFloat(100.55),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(1000.23),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloat(500.10),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
		},
	})
}

func TestAggregateByProductRatePlanCharge(t *testing.T) {
	testCase := []netsuite.JournalEntry{
		{
			Entity: "US",
			Lines: []revenue.TransferBatchJournalLine{
				{
					AccountedCredit:       decimal.NewFromFloat(100.55),
					AccountedDebit:        decimal.NewFromFloat(0),
					AccountingDate:        "AccountingDate",
					CurrencyCode:          "USD",
					Memo:                  "Memo",
					Entity:                "US",
					AccountingPeriodName:  "NOV-2020",
					GLAccountNumber:       "1004",
					ProductRatePlanCharge: "ABC",
				},
				{
					AccountedCredit:       decimal.NewFromFloat(1000.23),
					AccountedDebit:        decimal.NewFromFloat(0),
					AccountingDate:        "AccountingDate",
					CurrencyCode:          "USD",
					Memo:                  "Memo",
					Entity:                "US",
					AccountingPeriodName:  "NOV-2020",
					GLAccountNumber:       "1004",
					ProductRatePlanCharge: "ABC",
				},
				{
					AccountedCredit:       decimal.NewFromFloat(0),
					AccountedDebit:        decimal.NewFromFloat(500.10),
					AccountingDate:        "AccountingDate",
					CurrencyCode:          "USD",
					Memo:                  "Memo",
					Entity:                "US",
					AccountingPeriodName:  "NOV-2020",
					GLAccountNumber:       "1004",
					ProductRatePlanCharge: "ABC",
				},
				{
					AccountedCredit:       decimal.NewFromFloat(0),
					AccountedDebit:        decimal.NewFromFloat(69.10),
					AccountingDate:        "AccountingDate",
					CurrencyCode:          "USD",
					Memo:                  "Memo",
					Entity:                "US",
					AccountingPeriodName:  "NOV-2020",
					GLAccountNumber:       "1004",
					ProductRatePlanCharge: "ABCD",
				},
			},
		},
		{
			Entity: "AUS",
			Lines: []revenue.TransferBatchJournalLine{
				{
					AccountedCredit:       decimal.NewFromFloat(100),
					AccountedDebit:        decimal.NewFromFloat(0),
					AccountingDate:        "AccountingDate",
					CurrencyCode:          "USD",
					Memo:                  "Memo",
					Entity:                "AUS",
					AccountingPeriodName:  "NOV-2020",
					GLAccountNumber:       "1004",
					ProductRatePlanCharge: "ABC",
				},
				{
					AccountedCredit:       decimal.NewFromFloat(0),
					AccountedDebit:        decimal.NewFromFloat(200),
					AccountingDate:        "AccountingDate",
					CurrencyCode:          "USD",
					Memo:                  "Memo",
					Entity:                "AUS",
					AccountingPeriodName:  "NOV-2020",
					GLAccountNumber:       "1004",
					ProductRatePlanCharge: "ABC",
				},
			},
		},
	}

	testCaseUSExpectedResults := netsuite.JournalEntry{
		Entity: "US",
		Lines: []revenue.TransferBatchJournalLine{
			{
				AccountedCredit:       decimal.NewFromFloat(1100.78),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloat(500.10),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABCD",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloat(69.10),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "US",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABCD",
			},
		},
	}

	testCaseAUSExpectedResults := netsuite.JournalEntry{
		Entity: "AUS",
		Lines: []revenue.TransferBatchJournalLine{
			{
				AccountedCredit:       decimal.NewFromFloatWithExponent(100, 0),
				AccountedDebit:        decimal.NewFromFloat(0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "AUS",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
			{
				AccountedCredit:       decimal.NewFromFloat(0),
				AccountedDebit:        decimal.NewFromFloatWithExponent(200, 0),
				AccountingDate:        "AccountingDate",
				CurrencyCode:          "USD",
				Memo:                  "Memo",
				Entity:                "AUS",
				AccountingPeriodName:  "NOV-2020",
				GLAccountNumber:       "1004",
				ProductRatePlanCharge: "ABC",
			},
		},
	}

	USResult, _ := AggregateByProductRatePlanCharge(testCase[0])

	AUSResult, _ := AggregateByProductRatePlanCharge(testCase[1])

	USResultsJson, _ := json.MarshalIndent(USResult, "", "    ")
	USExpectedResultsJson, _ := json.MarshalIndent(testCaseUSExpectedResults, "", "    ")

	AUSResultsJson, _ := json.MarshalIndent(AUSResult, "", "    ")
	AUSExpectedResultsJson, _ := json.MarshalIndent(testCaseAUSExpectedResults, "", "    ")

	assert.Equal(t, string(USExpectedResultsJson), string(USResultsJson))
	assert.Equal(t, string(AUSExpectedResultsJson), string(AUSResultsJson))
}

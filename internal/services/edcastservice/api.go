package edcastservice

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/edcast"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
)

const (
	BaseUrl = "EDCAST_BASE_URL"
	ApiKey = "EDCAST_API_KEY"
	ApiSecret = "EDCAST_API_SECRET"
	AllowedReferrer = "EDCAST_ALLOWED_REFERRER"
)

var (
	jwt string
)

type authResponse struct {
	JwtToken string `json:"jwt_token"`
}

type EdCastAPI interface {
	FindUserById(id string) (user edcast.User, err error)
	CreateUser(user edcast.User) (err error)
}

func constructUrl(path string) (outUrl string, err error) {
	baseUrl := os.Getenv(BaseUrl)
	if len(baseUrl) == 0 {
		log.Println("EdCast base url is empty")
		return "", fmt.Errorf("EdCast base url is empty")
	}

	parsedUrl, err := url.ParseRequestURI(fmt.Sprintf("%s%s", baseUrl, path))
	if err != nil {
		return "", err
	}

	outUrl = parsedUrl.String()
	return outUrl, nil
}

func authenticate() (outjwt string, err error) {
	if len(jwt) > 0 {
		return jwt, nil
	}

	client := http.Client{}

	url, err:= constructUrl("/api/developer/v5/auth")
	if err != nil {
		return "", err
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)

	req.Header.Add("X-API-KEY", os.Getenv(ApiKey))
	req.Header.Add("X-AUTH-TOKEN", os.Getenv(ApiSecret))

	res, err := client.Do(req)
	if err != nil {
		return "", err
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}

	aRes := &authResponse{}

	err = json.Unmarshal(data, aRes)
	if err != nil {
		return "", err
	}

	jwt = aRes.JwtToken

	return aRes.JwtToken, nil
}

func authorizeRequest(req *http.Request) error {
	jwt, err := authenticate()
	if err != nil {
		return err
	}
	req.Header.Add("X-API-KEY", os.Getenv(ApiKey))
	req.Header.Add("X-ACCESS-TOKEN", jwt)
	return nil
}

func DoesUserExist(id string) (bool, error) {
	client := http.Client{}

	url, err:= constructUrl(fmt.Sprintf("/api/developer/v5/profiles/%s", id))
	if err != nil {
		return false, err
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)

	err = authorizeRequest(req)
	if err != nil {
		return false, err
	}

	// Fixes a bug with EdCast
	req.Header.Add("Accept", "*/*")

	res, err := client.Do(req)
	if err != nil {
		return false, err
	}

	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return false, err
	}

	resBody := &map[string]interface{}{}

	err = json.Unmarshal(data, resBody)
	if err != nil {
		return false, err
	}

	log.Println(resBody)

	if res.StatusCode != http.StatusOK {
		return false, nil
	}

	return true, nil
}

func CreateUser(user *edcast.User) (err error) {
	client := http.Client{}

	url, err:= constructUrl("/api/developer/v5/profiles")
	if err != nil {
		return err
	}

	requestPayload := &edcast.CreateUserRequest{Profiles: []edcast.User{*user}}

	rawData, err := json.Marshal(requestPayload)
	if err != nil {
		return err
	}

	payload := bytes.NewReader(rawData)

	req, err := http.NewRequest(http.MethodPost, url, payload)
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/json")
	err = authorizeRequest(req)
	if err != nil {
		return err
	}

	res, err := client.Do(req)
	if err != nil {
		return err
	}

	rawRes, _ := ioutil.ReadAll(res.Body)
	log.Println(string(rawRes))

	if res.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to create user in EdCast")
	}

	return nil
}

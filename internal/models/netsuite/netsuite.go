package netsuite

import (
	"fmt"
	"reflect"

	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/zuora/revenue"
)

type JournalEntry struct {
	Entity string                             `json:"entity"`
	Lines  []revenue.TransferBatchJournalLine `json:"lines"`
}

func (j *JournalEntry) GetHeaders() []string {
	var headers []string

	e := reflect.ValueOf(&revenue.TransferBatchJournalLine{}).Elem()

	for i := 0; i < e.NumField(); i++ {
		fieldName := e.Type().Field(i).Name
		headers = append(headers, fieldName)
	}

	return headers
}

func (j *JournalEntry) GetRows() [][]string {
	var rows [][]string

	for _, line := range j.Lines {
		var row []string

		e := reflect.ValueOf(&line).Elem()

		for i := 0; i < e.NumField(); i++ {
			fieldValue := e.Field(i).Interface()
			row = append(row, fmt.Sprintf(`%v`, fieldValue))
		}

		rows = append(rows, row)
	}

	return rows
}

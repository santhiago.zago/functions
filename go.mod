module gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions

go 1.13

require (
	github.com/ahmetb/go-linq v3.0.0+incompatible
	github.com/gorilla/schema v1.2.0 // indirect
	github.com/shopspring/decimal v1.2.0
	github.com/stretchr/testify v1.7.0
	gopkg.in/yaml.v2 v2.3.0
	sigs.k8s.io/yaml v1.2.0
)

package revenue

import (
	"fmt"

	"github.com/ahmetb/go-linq"
	"github.com/shopspring/decimal"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/netsuite"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/zuora/revenue"
)

func GroupByEntity(batch revenue.TransferFileBatch) ([]netsuite.JournalEntry, error) {
	journalEntriesByEntity := make(map[string]netsuite.JournalEntry)

	for _, journalLine := range batch.JournalLines {
		if val, present := journalEntriesByEntity[journalLine.Entity]; present {
			val.Lines = append(journalEntriesByEntity[journalLine.Entity].Lines, journalLine)
			journalEntriesByEntity[journalLine.Entity] = val
		} else {
			journalEntriesByEntity[journalLine.Entity] = netsuite.JournalEntry{
				Entity: journalLine.Entity,
				Lines: []revenue.TransferBatchJournalLine{
					journalLine,
				},
			}
		}
	}

	journalEntriesSlice := make([]netsuite.JournalEntry, 0)

	for _, je := range journalEntriesByEntity {
		journalEntriesSlice = append(journalEntriesSlice, je)
	}

	return journalEntriesSlice, nil
}

func AggregateByProductRatePlanCharge(entry netsuite.JournalEntry) (netsuite.JournalEntry, error) {
	var summaryEntry netsuite.JournalEntry

	lineGroups := make(map[string][]revenue.TransferBatchJournalLine)

	for _, line := range entry.Lines {
		if _, present := lineGroups[line.ProductRatePlanCharge]; present {
			lineGroups[line.ProductRatePlanCharge] = append(lineGroups[line.ProductRatePlanCharge], line)
		} else {
			lineGroups[line.ProductRatePlanCharge] = []revenue.TransferBatchJournalLine{
				line,
			}
		}
	}

	for _, lines := range lineGroups {
		var debitTotal, creditTotal decimal.Decimal

		for _, line := range lines {
			creditTotal = creditTotal.Add(line.AccountedCredit)
			debitTotal = debitTotal.Add(line.AccountedDebit)
		}

		summaryEntry.Entity = entry.Entity

		if summaryEntry.Lines == nil {
			summaryEntry.Lines = []revenue.TransferBatchJournalLine{}
		}

		summaryEntry.Lines = append(summaryEntry.Lines, revenue.TransferBatchJournalLine{
			AccountedCredit:       creditTotal,
			AccountedDebit:        decimal.NewFromFloat(0),
			AccountingDate:        lines[0].AccountingDate,
			CurrencyCode:          lines[0].CurrencyCode,
			Memo:                  lines[0].Memo,
			Entity:                lines[0].Entity,
			AccountingPeriodName:  lines[0].AccountingPeriodName,
			GLAccountNumber:       lines[0].GLAccountNumber,
			ProductRatePlanCharge: lines[0].ProductRatePlanCharge,
		})

		summaryEntry.Lines = append(summaryEntry.Lines, revenue.TransferBatchJournalLine{
			AccountedCredit:       decimal.NewFromFloat(0),
			AccountedDebit:        debitTotal,
			AccountingDate:        lines[0].AccountingDate,
			CurrencyCode:          lines[0].CurrencyCode,
			Memo:                  lines[0].Memo,
			Entity:                lines[0].Entity,
			AccountingPeriodName:  lines[0].AccountingPeriodName,
			GLAccountNumber:       lines[0].GLAccountNumber,
			ProductRatePlanCharge: lines[0].ProductRatePlanCharge,
		})
	}

	return summaryEntry, nil
}

func BatchToNSJEList(batchLines []revenue.TransferBatchJournalLine) []netsuite.JournalEntry {
	var summaryLines []netsuite.JournalEntry

	var entityGroups, prpcGroups, glAccountNumberGroups, creditDebitGroups []linq.Group

	// First group by Entity
	linq.From(batchLines).GroupByT(
		func(tl revenue.TransferBatchJournalLine) string { return tl.Entity },
		func(tl revenue.TransferBatchJournalLine) revenue.TransferBatchJournalLine { return tl },
	).ToSlice(&entityGroups)

	for _, entityGroup := range entityGroups {
		fmt.Printf("EntityId: %s\n", entityGroup.Key)

		summaryLine := netsuite.JournalEntry{
			Entity: entityGroup.Key.(string),
			Lines:  []revenue.TransferBatchJournalLine{},
		}

		// Next group by GL Account Code
		linq.From(entityGroup.Group).GroupByT(
			func(tl revenue.TransferBatchJournalLine) string { return tl.GLAccountNumber },
			func(tl revenue.TransferBatchJournalLine) revenue.TransferBatchJournalLine { return tl },
		).ToSlice(&glAccountNumberGroups)

		for _, glAccountNumberGroup := range glAccountNumberGroups {
			fmt.Printf("  Prpc: %s\n", glAccountNumberGroup.Key)

			// Next group by Product Rate Plan Charge
			linq.From(glAccountNumberGroup.Group).GroupByT(
				func(tl revenue.TransferBatchJournalLine) string { return tl.ProductRatePlanCharge },
				func(tl revenue.TransferBatchJournalLine) revenue.TransferBatchJournalLine { return tl },
			).ToSlice(&prpcGroups)

			for _, prpcGroup := range prpcGroups {
				fmt.Printf("    GLAccountNumber: %s\n", prpcGroup.Key)

				// Next group by credit or debit
				linq.From(prpcGroup.Group).GroupByT(
					func(tl revenue.TransferBatchJournalLine) bool {
						if !tl.AccountedCredit.IsZero() {
							return true
						} else {
							return false
						}
					},
					func(tl revenue.TransferBatchJournalLine) revenue.TransferBatchJournalLine {
						return tl
					},
				).ToSlice(&creditDebitGroups)

				for _, creditDebitGroup := range creditDebitGroups {
					// Aggregate debit and credit lines
					if creditDebitGroup.Key.(bool) == true {
						total := linq.From(creditDebitGroup.Group).AggregateWithSeedT(decimal.NewFromFloat(0),
							func(total decimal.Decimal, next revenue.TransferBatchJournalLine) decimal.Decimal {
								return total.Add(next.AccountedCredit)
							})

						fmt.Printf("      Credit: %s\n", total.(decimal.Decimal))

						ref := creditDebitGroup.Group[0].(revenue.TransferBatchJournalLine)

						summaryLine.Lines = append(summaryLine.Lines, revenue.TransferBatchJournalLine{
							AccountedCredit:       total.(decimal.Decimal),
							AccountedDebit:        decimal.NewFromFloat(0),
							AccountingDate:        ref.AccountingDate,
							CurrencyCode:          ref.CurrencyCode,
							Memo:                  ref.Memo,
							Entity:                ref.Entity,
							AccountingPeriodName:  ref.AccountingPeriodName,
							GLAccountNumber:       ref.GLAccountNumber,
							ProductRatePlanCharge: ref.ProductRatePlanCharge,
						})
					} else {
						total := linq.From(creditDebitGroup.Group).AggregateWithSeedT(decimal.NewFromFloat(0),
							func(total decimal.Decimal, next revenue.TransferBatchJournalLine) decimal.Decimal {
								return total.Add(next.AccountedDebit)
							})

						fmt.Printf("      Debit: %s\n", total.(decimal.Decimal))

						ref := creditDebitGroup.Group[0].(revenue.TransferBatchJournalLine)

						summaryLine.Lines = append(summaryLine.Lines, revenue.TransferBatchJournalLine{
							AccountedCredit:       decimal.NewFromFloat(0),
							AccountedDebit:        total.(decimal.Decimal),
							AccountingDate:        ref.AccountingDate,
							CurrencyCode:          ref.CurrencyCode,
							Memo:                  ref.Memo,
							Entity:                ref.Entity,
							AccountingPeriodName:  ref.AccountingPeriodName,
							GLAccountNumber:       ref.GLAccountNumber,
							ProductRatePlanCharge: ref.ProductRatePlanCharge,
						})
					}
				}
			}
		}

		summaryLines = append(summaryLines, summaryLine)
	}

	return summaryLines
}

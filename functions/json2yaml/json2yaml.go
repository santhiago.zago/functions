package json2yaml

import (
	http2 "gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/http"
	"io/ioutil"
	"net/http"
	yaml2 "sigs.k8s.io/yaml"
)

func Index(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
		return
	}

	yamlData, err := yaml2.JSONToYAML(data)

	if err != nil {
		http2.ReturnInternalServerError(w, err)
		return
	}

	w.Header().Set("Content-Type", "application/yaml")
	w.Write(yamlData)
}
package edcast

const (
	StatusActive = "active"
)

const(
	RoleMember = "member"
)

type User struct {
	Email string `json:"email"`
	FirstName string `json:"first_name"`
	LastName string `json:"last_name"`
	Id string `json:"external_id"`
	Status string `json:"status"`
	Roles []string `json:"roles"`
	ImpartnerAccount bool `json:"impartner_account"`
	SendEmailNotification bool `json:"send_email_notification"`
}

type CreateUserRequest struct {
	Profiles []User `json:"profiles"`
}
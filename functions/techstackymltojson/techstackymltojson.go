package techstackymltojson

import (
	"encoding/json"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/gitlab"
	http2 "gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/http"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	var techStack []gitlab.TechStackItem
	data, err := ioutil.ReadAll(r.Body)

	log.Print(r.Header)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
	}

	err = yaml.UnmarshalStrict(data, &techStack)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
	}

	resData, err := json.MarshalIndent(techStack, "", "  ")

	if err != nil {
		http2.ReturnBadRequestError(w, err)
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(resData)
}

package interfaces

type Csvable interface {
	GetHeaders() []string
	GetRows() [][]string
}

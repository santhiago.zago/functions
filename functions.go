package functions

import (
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/edcastJitUserCreateProxy"
	"net/http"

	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/RoundAndBalanceNetsuiteJE"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/hextobase64"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/json2yaml"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/techstackymltojson"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/functions/zuorarevenue"
)

func TechstackYmlToJson(w http.ResponseWriter, r *http.Request) {
	techstackymltojson.Index(w, r)
}

func TransferFileToNSJE(w http.ResponseWriter, r *http.Request) {
	zuorarevenue.TransferFileToNSJE(w, r)
}

func RoundBalanceJE(w http.ResponseWriter, r *http.Request) {
	RoundAndBalanceNetsuiteJE.Index(w, r)
}

func Json2Yaml(w http.ResponseWriter, r *http.Request) {
	json2yaml.Index(w, r)
}

func HexToBase64(w http.ResponseWriter, r *http.Request) {
	hextobase64.Index(w, r)
}

func EdCastJIT(w http.ResponseWriter, r *http.Request) {
	edcastJitUserCreateProxy.Index(w, r)
}

package misc

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestDecodeHex(t *testing.T) {
	input := []byte("48656c6c6f20576f726c64")
	expectedOutput := "Hello World"
	out, _ := DecodeHex(input)
	assert.Equal(t, expectedOutput, string(out))
}

func TestEncodeBase64(t *testing.T) {
	input := []byte("Hello World")
	out := Base64Encode(input)
	assert.Equal(t, []uint8([]byte{0x53, 0x47, 0x56, 0x73, 0x62, 0x47, 0x38, 0x67, 0x56, 0x32, 0x39, 0x79, 0x62, 0x47, 0x51, 0x3d}), out)
}

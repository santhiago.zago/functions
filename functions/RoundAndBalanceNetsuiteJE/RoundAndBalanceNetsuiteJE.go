package RoundAndBalanceNetsuiteJE

import (
	"encoding/csv"
	"encoding/json"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/internal/models/netsuite"
	http2 "gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/http"
	netsuite2 "gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/netsuite"
	"io/ioutil"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	journalEntry := new(netsuite.JournalEntry)
	data, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
		return
	}

	err = json.Unmarshal(data, journalEntry)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
		return
	}

	balancedJournalEntry := netsuite2.RoundAndBalanceJournalEntry(*journalEntry)

	var rawResponse []byte

	if r.URL.Query()["csv"] != nil {
		w.Header().Set("Content-Type", "text/csv")
		csvWriter := csv.NewWriter(w)
		csvWriter.Write(balancedJournalEntry.GetHeaders())
		csvWriter.WriteAll(balancedJournalEntry.GetRows())
	} else {
		w.Header().Set("Content-Type", "application/json")
		rawResponse, _ = json.MarshalIndent(balancedJournalEntry, "", "  ")
		w.Write(rawResponse)
	}
}
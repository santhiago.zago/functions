package http

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"net/http"
	"testing"
)

type mockResponseWriter struct {
	mock.Mock
}

func (m *mockResponseWriter) WriteHeader(statusCode int) {
	m.Called(statusCode)
}

func (m *mockResponseWriter) Write(data []byte) (int, error) {
	args := m.Called(data)
	return args.Int(0), args.Error(1)
}

func (m *mockResponseWriter) Header() http.Header {
	return http.Header{}
}

func TestReturnBadRequestError(t *testing.T) {
	err := fmt.Errorf("this is an error")

	w := new(mockResponseWriter)
	w.On("WriteHeader", 400)
	w.On("Write", []byte(err.Error())).Return(16, nil)

	ReturnBadRequestError(w, err)
	assert.Equal(t, 2, len(w.Calls))
	w.AssertCalled(t, "WriteHeader", 400)
	w.AssertCalled(t, "Write", []byte(err.Error()))
}

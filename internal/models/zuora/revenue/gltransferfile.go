package revenue

import "github.com/shopspring/decimal"

type TransferFileBatch struct {
	BatchCount   int                        `json:"batchCount"`
	BatchId      int                        `json:"batchId"`
	JournalLines []TransferBatchJournalLine `json:"journalLines"`
}

type TransferBatchJournalLine struct {
	AccountedCredit       decimal.Decimal `json:"accountedCredit"`
	AccountedDebit        decimal.Decimal `json:"accountedDebit"`
	AccountingDate        string          `json:"accountingDate"`
	CurrencyCode          string          `json:"currencyCode"`
	Memo                  string          `json:"memo"`
	Entity                string          `json:"entity"`
	AccountingPeriodName  string          `json:"accountingPeriodName"`
	GLAccountNumber       string          `json:"glAccountNumber"`
	ProductRatePlanCharge string          `json:"productRatePlanCharge"`
}

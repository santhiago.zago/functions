package hextobase64

import (
	"io/ioutil"
	"net/http"

	http2 "gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/http"
	"gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations/functions/lib/misc"
)

func Index(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodPost {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	hexData, err := ioutil.ReadAll(r.Body)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
		return
	}

	db, err := misc.DecodeHex(hexData)

	if err != nil {
		http2.ReturnBadRequestError(w, err)
		return
	}

	response := misc.Base64Encode(db)

	w.Header().Set("Content-Type", "text/plain")
	w.Write(response)
}

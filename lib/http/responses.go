package http

import (
	"log"
	"net/http"
)

func ReturnBadRequestError(w http.ResponseWriter, err error) {
	log.Print(err.Error())
	w.WriteHeader(http.StatusBadRequest)
	w.Write([]byte(err.Error()))
}

func ReturnInternalServerError(w http.ResponseWriter, err error) {
	log.Print(err.Error())
	w.WriteHeader(http.StatusInternalServerError)
	w.Write([]byte(err.Error()))
}
